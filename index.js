const DEVELOPMENT = 'development'
const STAGING = 'staging'
const PRODUCTION = 'production'

module.exports = function (env) {
  env = env || process.env.NODE_ENV || PRODUCTION

  let shortName
  let caps
  let ahau
  let pataka

  switch (env) {
    case DEVELOPMENT:
      shortName = 'dev'
      caps = {
        shs: 'MPqTil36buMt6/H35qxmid0+WDLthPzr3J0ap7SAINg=',
        sign: 'JVmrhu7T+dHXvu7M9MErUXOTwVKI7bvkwz5BHCQjx3s='
      }
      ahau = {
        appName: 'ahau-dev',
        shortName,
        caps,
        port: 8067,
        serveBlobs: {
          port: 28067
        },
        hyperBlobs: {
          port: 38067
        },
        graphql: {
          port: 18067
        }
      }
      pataka = {
        appName: 'ahau-pataka-dev',
        shortName,
        caps,
        port: 8068,
        serveBlobs: {
          port: 28068
        },
        hyperBlobs: {
          port: 38068
        },
        graphql: {
          port: 18068
        }
      }
      break

    case STAGING:
      shortName = 'staging'
      caps = {
        shs: 'nKTY24KY9ove+0AHbtgb6aKwPe8I9A1eS3qh0Fh2jk4=',
        sign: 'dNqmCGnKYOxi8taShJkWZiM1xlwBskm6biAM5VlmFD8='
      }
      ahau = {
        appName: 'ahau-staging',
        caps,
        port: 8077,
        serveBlobs: {
          port: 28077
        },
        hyperBlobs: {
          port: 38077
        },
        graphql: {
          port: 18077
        }
      }
      pataka = {
        appName: 'ahau-pataka-staging',
        caps,
        port: 8078,
        serveBlobs: {
          port: 28078
        },
        hyperBlobs: {
          port: 38078
        },
        graphql: {
          port: 18078
        }
      }
      break

    case PRODUCTION:
      shortName = 'prod'
      caps = {
        shs: 'Uqx6Yplq3C1jHKhyZb+JIYyLGo8Vz/vouUvpzrecXSE=',
        sign: '3G5twFhQ3z47ccPsBH3eFrYWYZnbt8hLqW9lWNk3K+o='
      }
      ahau = {
        appName: 'ahau',
        caps,
        port: 8087,
        serveBlobs: {
          port: 28087
        },
        hyperBlobs: {
          port: 38087
        },
        graphql: {
          port: 18087
        }
      }
      pataka = {
        appName: 'ahau-pataka',
        caps,
        port: 8088,
        serveBlobs: {
          port: 28088
        },
        hyperBlobs: {
          port: 38088
        },
        graphql: {
          port: 18088
        }
      }
      break

    default:
      throw new Error(`Invalid NODE_ENV: "${env}"`)
  }

  return {
    name: env,
    shortName,
    ahau,
    pataka,

    isDevelopment: env === DEVELOPMENT,
    isStaging: env === STAGING,
    isProduction: env === PRODUCTION
  }
}
